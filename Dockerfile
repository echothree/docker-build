FROM amazoncorretto:17-al2-jdk

ARG ANT_VERSION=1.10.12
ARG WILDFLY_VERSION=26.1.0.Final

ENV ANT_HOME=/opt/ant
ENV WILDFLY_HOME=/opt/wildfly
ENV PATH=$PATH:$ANT_HOME/bin:$WILDFLY_HOME/bin

RUN yum update -y \
  && yum install -y tar gzip git \
  && curl -O https://downloads.apache.org/ant/binaries/apache-ant-$ANT_VERSION-bin.tar.gz \
  && tar -xzf apache-ant-$ANT_VERSION-bin.tar.gz \
  && mv apache-ant-$ANT_VERSION $ANT_HOME \
  && rm apache-ant-$ANT_VERSION-bin.tar.gz \
  && curl -L -O https://github.com/wildfly/wildfly/releases/download/$WILDFLY_VERSION/wildfly-$WILDFLY_VERSION.tar.gz \
  && tar -xzf wildfly-$WILDFLY_VERSION.tar.gz \
  && mv wildfly-$WILDFLY_VERSION $WILDFLY_HOME \
  && rm wildfly-$WILDFLY_VERSION.tar.gz \
  && curl -O https://repo.mysql.com/mysql80-community-release-el7-5.noarch.rpm \
  && yum install -y mysql80-community-release-el7-5.noarch.rpm \
  && rm mysql80-community-release-el7-5.noarch.rpm \
  && yum install -y mysql-community-server \
  && /usr/sbin/mysqld --initialize --user=mysql \
  && MYSQL_ROOT_PASSWORD=`grep 'temporary password' /var/log/mysqld.log  | sed 's/.*: \\(.*\\)/\\1/g'` \
  && echo $MYSQL_ROOT_PASSWORD > mysql-root-password.txt
